import React, { Component } from "react";

import ReactMapboxGl, {
  ZoomControl,
  Marker,
  ScaleControl,
  Cluster,
  Layer,
  Feature
} from "react-mapbox-gl";

import DrawControl from "react-mapbox-gl-draw";

import Header from "./components/header";
import {styles} from './theme';

import "./App.css";

//draw import
require("@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css");

const onDrawCreate = ({ features }) => {
  console.log(features);
};
const onDrawUpdate = ({ features }) => {
  console.log(features);
};

 
const Map = new ReactMapboxGl({
  accessToken:
    "pk.eyJ1IjoiYnVyYWthbHAiLCJhIjoiY2s1MmJuenN0MDV6bjNkcWVmdno1cXVrdSJ9.8gbi3yteXnkOdh2SG2ETZw",
  attributionControl: false
});


const places = require("./falls.json");

const markerUrl =
  "https://demo.bulutfilo.com/images/map-markers/gray-minibus.svg";

//polygon-çizim
const AllShapesMultiPolygonCoords = [
  [
    [
      [29.850027619153423, 40.75648681281257],
      [29.839727936536434, 40.74387251606905],
      [29.891226349622713, 40.73840991325139],
      [29.908220825940532, 40.75544654898425],
      [29.850027619153423, 40.75648681281257]
    ]
  ],
  [
    [
      [-0.18235092163085938, 51.538250335096376],
      [-0.1674163818359375, 51.54433860667918],
      [-0.15591506958007812, 51.53974577545329],
      [-0.15831832885742188, 51.53429786349477],
      [-0.17531280517578122, 51.53429786349477],
      [-0.18200759887695312, 51.537823057404094]
    ]
  ]
];

//arac gemisi
const route1 = {
  type: "geojson",
  data: {
    geometry: {
      type: "LineString",
      coordinates: [
        [30.012593, 40.791557],
        [30.010913, 40.790018],
        [30.010913, 40.790018],
        [30.004902, 40.789465],
        [30.000888, 40.789555],
        [29.993217, 40.788728],
        [29.98691, 40.787248]
      ]
    }
  }
};

const route2 = {
  type: "geojson",
  data: {
    geometry: {
      type: "LineString",
      coordinates: [
        [29.98691, 40.787248],
        [29.975007, 40.785283],
        [29.974912, 40.785048],
        [29.969612, 40.77433],
        [29.966617, 40.76274],
        [29.966877, 40.761277],
        [29.964337, 40.761728],
        [29.9604, 40.76226],
        [29.956307, 40.762475],
        [29.941108, 40.762147],
        [29.927162, 40.761407],
        [29.922215, 40.761368],
        [29.90963, 40.765853],
        [29.895382, 40.764602],
        [29.878568, 40.762983],
        [29.875793, 40.763595],
        [29.871432, 40.761797],
        [29.854593, 40.761647],
        [29.850887, 40.76087],
        [29.84155, 40.75818],
        [29.83467, 40.756233],
        [29.828368, 40.75571],
        [29.828492, 40.755317],
        [29.828565, 40.755322],
        [29.827985, 40.757083],
        [29.82469, 40.757183],
        [29.822933, 40.759328]
      ]
    }
  }
};

const lineLayout = {
  "line-cap": "round",
  "line-join": "round"
};

const linePaint = {
  "line-color": "black",
  "line-width": 5,
  "line-gradient": [
    "interpolate",
    ["linear"],
    ["line-progress"],
    0,
    "yellow",
    1,
    "red"
  ]
};

const polygonPaint = {
  "fill-color": "red",
  "fill-opacity": 0.2
};

interface Point {
  lat(): number;
  lng(): number;
}

interface Route {
  [key: string]: any;
  points: Point[];
}

const route: Route = require("./route.json");

const mappedRoute = route.points.map(point => [point.lng, point.lat]);


var clusterMarker = function(coordinates, pointCount, getLeaves) {
  var style = clusterGreen;
  if (pointCount >= 3 && pointCount <= 6) {
    style = clusterYellow;
  } else if (pointCount >= 7) {
    style = clusterBrown;
  }
  return (
    <Marker
      className="clusterMarkerStyle"
      coordinates={coordinates}
      style={style}
    >
      <div className="pointCount">{pointCount}</div>
    </Marker>
  );
};

class App extends Component {
  constructor(props) {
    super(props);
    this.map = React.createRef();
    this.state = {
      initialState: styles.google,
      zoom: [13],
      center: [29.975232, 40.763964],
      routeIndex: 0,
      circleRadius: 9,
      animationStep:50
    };
  }

  getCirclePaint = () => ({
    "circle-radius": this.state.circleRadius,
    "circle-color": "#E54E52",
    "circle-opacity": 0.8
  });

  //tema değişimi  
  switchLayer(layer) {
    this.setState({ initialState: styles[layer] });
  }

  //line-animasyon
  enableLineAnimation(layerId) {
    console.log(this.map.current.state.map);
    var step = 0;
    let dashArraySeq = [
      [0, 4, 3],
      [1, 4, 2],
      [2, 4, 1],
      [3, 4, 0],
      [0, 1, 3, 3],
      [0, 2, 3, 2],
      [0, 3, 3, 1]
    ];
    setInterval(() => {
      step = (step + 1) % dashArraySeq.length;
      this.map.current.state.map.setPaintProperty(
        layerId,
        "line-dasharray",
        dashArraySeq[step]
      );
    }, this.state.animationStep);
  }

  render() {
    return (
      <div className="App">
        <Header />
        <div className="layerConsole">
          <p onClick={() => this.switchLayer("google")}>Google Sokak</p>
          <p onClick={() => this.switchLayer("googleHybrid")}>Google Hybrid</p>
          <p onClick={() => this.switchLayer("here")}>Here Sokak</p>
          <p onClick={() => this.switchLayer("mapbox")}>Mapbox</p>
          <p onClick={() => this.switchLayer("hgmDefault")}>HGM Default</p>
          <p onClick={() => this.switchLayer("hgmDark")}>HGM Dark</p>
          <p onClick={() => this.switchLayer("hgmUydu")}>HGM Uydu</p>
        </div>
        <Map
          ref={this.map}
          zoom={this.state.zoom}
          center={this.state.center}
          style={this.state.initialState}
          containerStyle={{
            height: "100vh",
            width: "100vw"
          }}
        >
          {
            <Cluster zoomOnClick="true" ClusterMarkerFactory={clusterMarker}>
              {places.features.map((feature, key) => (
                <Marker
                  key={key}
                  coordinates={feature.geometry.coordinates}
               
                  className="item"
                  anchor="bottom"
                >
                  <div style={car}>
                    <span className="code">{feature.properties.name}</span>
                    <div className="status"></div>
                  </div>
                </Marker>
              ))}
            </Cluster>
          }
          <DrawControl
            onDrawCreate={onDrawCreate}
            onDrawUpdate={onDrawUpdate}
          />
          <ZoomControl />
          <ScaleControl />
          <Layer type="circle" paint={this.getCirclePaint()}>
            <Feature coordinates={mappedRoute[this.state.routeIndex]} />
          </Layer>
          <Layer type="symbol" layout={{ "icon-image": "harbor-15" }}></Layer>
          <Marker
            coordinates={mappedRoute[this.state.routeIndex + 9]}
            className="item"
            anchor="bottom"
          >
            <div style={car}>
              <span>test </span>
              <div className="status"></div>
            </div>
          </Marker>

          <Layer type="fill" paint={polygonPaint}>
            <Feature coordinates={AllShapesMultiPolygonCoords} />
          </Layer>

          <Layer
            id="route1"
            onClick={() => this.enableLineAnimation("route1")}
            type="line"
            layout={lineLayout}
            paint={linePaint}
            geoJSONSourceOptions={{ lineMetrics: true }}
          >
            <Feature coordinates={route1.data.geometry.coordinates} />
          </Layer>

          <Layer
            id="route2"
            onClick={() => this.enableLineAnimation("route2")}
            type="line"

            layout={lineLayout}
            paint={linePaint}
            geoJSONSourceOptions={{ lineMetrics: true }}
          >
            <Feature coordinates={route2.data.geometry.coordinates} />
          </Layer>

        </Map>
      </div>
    );
  }
}


const car = {
  display: "flex",
  height: "30px",
  width: "30px",
  alignItems: "center",
  backgroundImage: `url(${markerUrl})`,
  backgroundSize: "contain",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center center"
};


const clusterGreen = {
  backgroundColor: "#70C680",
  borderColor: "#97EBA7"
};

const clusterYellow = {
  backgroundColor: "#C9C66C",
  borderColor: "#EBE197"
};

const clusterBrown = {
  backgroundColor: "#C98E78",
  borderColor: "#EBB197"
};

export default App;
