export const styles = {
  google: {
    version: 8,
    sources: {
      tiles: {
        type: "raster",
        tiles: ["https://mt3.google.com/vt/lyrs=m&z={z}&x={x}&y={y}"],
        tileSize: 256,
        attribution: ""
      }
    },
    layers: [
      {
        id: "base-layer",
        type: "raster",
        source: "tiles",
        minzoom: 0,
        maxzoom: 22
      }
    ]
  },
  googleHybrid: {
    version: 8,
    sources: {
      tiles: {
        type: "raster",
        tiles: ["https://mt3.google.com/vt/lyrs=y&z={z}&x={x}&y={y}"],
        tileSize: 256,
        attribution: ""
      }
    },
    layers: [
      {
        id: "base-layer",
        type: "raster",
        source: "tiles",
        minzoom: 0,
        maxzoom: 22
      }
    ]
  },
  here: {
    version: 8,
    sources: {
      tiles: {
        type: "raster",
        tiles: [
          "http://1.map.takip724.com/?type=heremap&t=s&z={z}&x={x}&y={y}"
        ],
        tileSize: 256,
        attribution: ""
      }
    },
    layers: [
      {
        id: "base-layer",
        type: "raster",
        source: "tiles",
        minzoom: 0,
        maxzoom: 22
      }
    ]
  },
  mapbox: "mapbox://styles/mapbox/streets-v11",
  hgmDefault: "http://harita.citysurf.com.tr/style/hgm_varsayilan.json",
  hgmDark: "http://harita.citysurf.com.tr/style/hgm_dark.json",
  hgmUydu: "http://harita.citysurf.com.tr/style/hgm_uydu.json"
};

export default {
  styles,
}